package hw3;

import javafx.application.Application;
import javafx.geometry.Insets;
import javafx.scene.Scene;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.Pane;
import javafx.scene.paint.Color;
import javafx.scene.shape.Rectangle;
import javafx.scene.text.Text;
import javafx.stage.Stage;

import java.util.Scanner;

/**
 * Two Rectangles: Exercise 14.23. Prompt the user to enter center coordinates, width, and height of two rectangles.
 * Display the rectangles and a message indicating whether the rectangles overlap, one contains another, or do not
 * overlap.
 * @author Katya Pilkington
 */
public class TwoRectangles extends Application {

    /**
     * Primary method that takes care of most of the JavaFX logic. Sets the panes, nodes, scene, and stange.
     * @param primaryStage: start method takes a stage argument.
     */
    public void start(Stage primaryStage){
        // BorderPane allows for padding, placement of rectangles and message
        BorderPane mainPane = new BorderPane();

        // Create default pane for rectangles to be added to.
        Pane rectanglePane = new Pane();

        // Get rectangles from getRectangle method, make them 2 different colors to distinguish.
        Rectangle rectangle1 = getRectangle("Rectangle1");
        rectangle1.setFill(Color.TRANSPARENT);
        rectangle1.setStroke(Color.CORAL);
        rectanglePane.getChildren().add(rectangle1);

        Rectangle rectangle2 = getRectangle("Rectangle2");
        rectangle2.setFill(Color.TRANSPARENT);
        rectangle2.setStroke(Color.CORNFLOWERBLUE);
        rectanglePane.getChildren().add(rectangle2);

        mainPane.setCenter(rectanglePane);

        // Use checkOverlap to figure out how the rectangles are situated in relation to each other and what message to
        // display at the bottom of the window.
        Text message = new Text(checkOverlap(rectangle1, rectangle2));
        mainPane.setBottom(message);

        // Set padding to look nice.
        mainPane.setPadding(new Insets(10, 20, 10, 20));

        Scene scene = new Scene(mainPane);

        primaryStage.setTitle("Two Rectangles: Exercise 14.23");

        primaryStage.setScene(scene);

        primaryStage.show();
    }

    /**
     * Get user input to create Rectangle nodes.
     * @param name: name of the rectangle to be used in print statement prompts.
     * @return new Rectangle object based values given from the console
     */
    private Rectangle getRectangle(String name){
        Scanner scanner = new Scanner(System.in);

        // Prompt user to enter all required values to create a Rectangle node.
        System.out.println("Enter a center x for " + name + ".");
        double centerX = scanner.nextDouble();

        System.out.println("Enter a center y for " + name + ".");
        double centerY = scanner.nextDouble();

        System.out.println("Enter a width for " + name + ".");
        double width = scanner.nextDouble();

        System.out.println("Enter a height  for " + name + ".");
        double height = scanner.nextDouble();


        // Prompt user for different rectangle values if the entire rectangle cannot be displayed based on given values.
        while (centerX < (width / 2)) {
            System.out.println("The center x & width specified cannot be displayed. Please choose different values.");

            System.out.println("Enter a center x for " + name + ".");
            centerX = scanner.nextDouble();

            System.out.println("Enter width for " + name + ".");
            width = scanner.nextDouble();
        }

        while (centerY < (height / 2)){
            System.out.println("The center y & height specified cannot be displayed. Please choose different values.");

            System.out.println("Enter a center y for " + name + ".");
            centerY = scanner.nextDouble();

            System.out.println("Enter a height for " + name + ".");
            height = scanner.nextDouble();
        }

        // Return a new Rectangle object with (x,y) coordinates being the top left corner of the rectangle.
        return new Rectangle(centerX - (width / 2), centerY - (height / 2), width, height);
    }

    /**
     * Determine if two rectangles from Rectangle nodes overlap, or one rectangle is contained in the other.
     * @param rectangle1: first rectangle
     * @param rectangle2: second rectangle
     * @return String statement to be used as the message of if/how the rectangles overlap.
     */
    private String checkOverlap(Rectangle rectangle1, Rectangle rectangle2){
        // Setting variables so I don't have to keep calling getter functions
        double x1 = rectangle1.getX();
        double y1 = rectangle1.getY();
        double height1 = rectangle1.getHeight();
        double width1 = rectangle1.getWidth();

        double x2 = rectangle2.getX();
        double y2 = rectangle2.getY();
        double height2 = rectangle2.getHeight();
        double width2 = rectangle2.getWidth();

        // If the distance from the top left corners in the x and y directions are within the width and height of
        // at least one of the rectangles, the rectangles overlap.
        if(( Math.abs(x1 - x2) <= width1 || Math.abs(x1 - x2) <= width2 ) &&
                ( Math.abs(y1 - y2) <= height1 || Math.abs(y1 - y2) <= height2 )){

            // Rectangle 2 is smaller and contained in Rectangle 1.
            if ((x1 <= x2) && (y1 <= y2) && (x1 + width1 >= x2 + width2) && (y1 - height1 >= y2 - height2)){
                return "One rectangle is contained in another";
            }

            // Rectangle 1 is smaller and contained in Rectangle 2.
            else if((x2 <= x1) && (y2 <= y1) && (x2 + width2 >=  x1 + width1) && (y2 - height2 >= y1 - height1)){
                return "One rectangle is contained in another";
            }

            // The rectangles just overlap: one does not contain the other.
            else {
                return "The rectangles overlap";
            }
        }

        return "The rectangles do not overlap";
    }

    public static void main(String[] args) {
        launch(args);
    }

}
