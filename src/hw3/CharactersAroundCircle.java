package hw3;

import javafx.application.Application;
import javafx.geometry.Insets;
import javafx.scene.Scene;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.Pane;
import javafx.scene.text.Font;
import javafx.scene.text.FontWeight;
import javafx.scene.text.Text;
import javafx.stage.Stage;

/**
 * Exercise 14.5: Display the message WELCOME TO JAVA around a circle.
 * @author Katya Pilkinton
 */
public class CharactersAroundCircle extends Application {
    /**
     * Start method takes care of pane, scene, and stage placement. Calls circleCharacters to actually create and place
     * Text nodes.
     * @param primaryStage : stage argument as required of the start method
     */
    public void start(Stage primaryStage){
        // Use border pane to set padding
        BorderPane borderPane = new BorderPane();

        // Call circleCharacters with separate message, pane, and radius variables for increased functionality / easy
        // changes
        Pane circlePane = new Pane();
        String message = "WELCOME TO JAVA ";
        double radius = 75;
        circleCharacters(message, circlePane, radius);

        // Place pane with circle message on borderPane and add padding based on radius of the circle.
        borderPane.setCenter(circlePane);
        borderPane.setPadding(new Insets(radius / 2,radius / 2,radius / 4,radius / 2));

        Scene scene = new Scene(borderPane);

        primaryStage.setTitle("Characters around circle: Exercise 14.5");

        primaryStage.setScene(scene);

        primaryStage.show();
    }

    /**
     * Actually create and place Text nodes on a pane around in a circle shape. Calculate placement and rotation of
     * individual characters.
     * @param message : string message to be displayed around the circle
     * @param textPane : pane to place Text nodes
     * @param radius : how big the circle is
     */
    private void circleCharacters(String message, Pane textPane, double radius){
        // Calculate portion of circle per character in the string message. We will need it in radians and degrees.
        double radiansFraction = (2 * Math.PI) / message.length();
        double degreeFraction = 360.0 / message.length();

        // loop through string message to place each character as an individual Text node.
        for (int i = 0; i < message.length(); i++) {
            // calculate x,y placement of character based on trig.
            Text character = new Text((Math.cos(radiansFraction * i) * radius) + (1 *  radius),
                    (Math.sin(radiansFraction * i) * radius) + (1 * radius), message.charAt(i) + "");

            // Rotate each of the characters so that their bases point to the center of the circle.
            character.setRotate((degreeFraction * i) + 90);
            character.setFont(Font.font("Verdana", FontWeight.BOLD, 20));
            textPane.getChildren().add(character);
        }
    }

    public static void main(String[] args) {
        launch(args);
    }
}
