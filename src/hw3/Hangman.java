package hw3;

import javafx.application.Application;
import javafx.geometry.Insets;
import javafx.scene.Scene;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.Pane;
import javafx.scene.paint.Color;
import javafx.scene.shape.Arc;
import javafx.scene.shape.ArcType;
import javafx.scene.shape.Circle;
import javafx.scene.shape.Line;
import javafx.stage.Stage;

/**
 * GAME Hangman: Exercise 14.17: Display a drawing for a hangman game
 * I decided to have some fun with naming my methods.
 * @author Katya Pilkington
 */
public class Hangman extends Application {
    public void start(Stage primaryStage){
        BorderPane mainPane = new BorderPane();

        Pane hangman = new Pane();
        letTheBodyDrop(hangman);

        mainPane.setCenter(hangman);
        mainPane.setPadding(new Insets(10, 20, 10,20));

        Scene scene = new Scene(mainPane);

        primaryStage.setTitle("Game Hangman: Exercise 14.17");
        primaryStage.setScene(scene);

        primaryStage.show();
    }

    // Draw the stand using 3 lines and an arc.
    private void justHangin(Pane pane){
        pane.getChildren().add(new Line(50, 10, 50, 450));
        pane.getChildren().add(new Line(50,10,200,10));
        pane.getChildren().add(new Line(200,10,200,50));

        Arc arc = new Arc(50, 470, 50, 20, 0, 180);
        arc.setType(ArcType.OPEN);
        arc.setFill(Color.TRANSPARENT);
        arc.setStroke(Color.BLACK);
        pane.getChildren().add(arc);
    }

    // Draw the head using a Circle node.
    private void getAHead(Pane pane){
        Circle head = new Circle(200, 90, 40);
        head.setFill(Color.TRANSPARENT);
        head.setStroke(Color.BLACK);
        pane.getChildren().add(head);
    }

    // Draw body using a Line.
    private void iGotNoBody(Pane pane){
        Line body = new Line(200,130,200, 280);
        pane.getChildren().add(body);
    }

    // Draw left arm as a Line.
    private void noArmLeftBehind(Pane pane){
        Line leftArm = new Line(200, 150, 150, 250);
        pane.getChildren().add(leftArm);
    }

    // Draw right arm as a Line.
    private void rightToArmBears(Pane pane){
        Line rightArm = new Line(200, 150, 250, 250);
        pane.getChildren().add(rightArm);
    }

    // Draw right leg as a Line.
    private void rightFootLetsStomp(Pane pane){
        Line rightLeg = new Line(200,280,150,400);
        pane.getChildren().add(rightLeg);
    }

    // Draw left leg as a Line.
    private void twoLeftFeet(Pane pane){
        Line leftLeg = new Line(200,280,250,400);
        pane.getChildren().add(leftLeg);
    }

    // Method draws entire hangman picture at once by calling each method in order.
    private void letTheBodyDrop(Pane pane){
        justHangin(pane);
        getAHead(pane);
        iGotNoBody(pane);
        noArmLeftBehind(pane);
        rightToArmBears(pane);
        rightFootLetsStomp(pane);
        twoLeftFeet(pane);
    }

    public static void main(String[] args) {
        launch(args);
    }
}
