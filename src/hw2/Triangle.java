package hw2;

public class Triangle extends Shape{
    public Triangle(){
        side = 0;
    }

    public Triangle(double side){
        this.side = side;
    }

    // Returns surface area of an equilateral triangle with given side length.
    public double getArea(){
       return side * side * Math.sqrt(3) / 4;
    }
}
