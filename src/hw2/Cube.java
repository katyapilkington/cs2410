package hw2;

public class Cube extends Shape{
    public Cube(){
        side = 0;
    }

    public Cube(double side){
        this.side = side;
    }

    // Return surface area of the cube, which is 6 squares with given side.
    public double getArea(){
        return side * side * 6;
    }
}
