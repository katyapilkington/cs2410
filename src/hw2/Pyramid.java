package hw2;

/**
 * Pyramid class forms 3d shape made up of 4 triangles (triangular-base pyramid instead of square base).
 */
public class Pyramid extends Shape{
    public Pyramid(){
        side = 0;
    }

    public Pyramid(double side){
        this.side = side;
    }

    // Returns the surface area of the pyramid, which is equal to 4 triangles with given side length.
    public double getArea(){
        return side * side * Math.sqrt(3);
    }
}
