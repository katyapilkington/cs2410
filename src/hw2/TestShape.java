package hw2;

import java.util.ArrayList;

/**
 * Homework 2: Polymorphism
 * TestShape demonstrates the polymorphic properties of Shape and its children classes. An ArrayList of 8 Shape
 * objects (2 for each child class) is looped through to call the abstract getArea() method on each child object, with
 * the results printed.
 * @author Katya Pilkington
 */
public class TestShape {
    public static void main(String[] args) {
        double side1 = 1.5;
        double side2 = 5.75;
        
        // Initialize ArrayList and add two objects of each child class.
        ArrayList<Shape> shapes = new ArrayList<>();
        shapes.add(new Square(side1));
        shapes.add(new Square(side2));
        shapes.add(new Cube(side1));
        shapes.add(new Cube(side2));
        shapes.add(new Triangle(side1));
        shapes.add(new Triangle(side2));
        shapes.add(new Pyramid((side1)));
        shapes.add(new Pyramid(side2));
        
        // Loop through the ArrayList and print out the result of getArea() called on each object.
        for (Shape shape : shapes) {
            System.out.print("The " + shape.getClass().getSimpleName() + " with side " + shape.getSide()
                    + " has an area of ");
            System.out.printf("%.2f\n", shape.getArea());
            
        }
        

     }
}
