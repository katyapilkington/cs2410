package hw2;
import javafx.application.Application;
import javafx.scene.text.Font;
import javafx.stage.Stage;
import javafx.scene.Scene;
import javafx.scene.text.Text;
import javafx.scene.paint.Color;
import javafx.scene.layout.Pane;

/**
 * Homework 2: Hello World JavaFX GUI
 * A JavaFX application with a stage, scene, pane, and text that says "Hello World!", with hello and world being two
 * different colors.
 * @author Katya Pilkington
 */
// JavaFX extends application.
public class HelloWorld extends Application {
    @Override
    // Override the Application start method with a stage argument
    public void start(Stage primaryStage) {
        // Create a new pane that the text will be placed on.
        Pane pane = new Pane();

        // To set the different words as different colors, I made two text nodes hello and world. Both have the same
        // font and size, and their positions are bound to the size of the window.
        Text hello = new Text();
        hello.setFill(Color.CORNFLOWERBLUE);
        hello.setText("Hello ");
        hello.setFont(Font.font("Verdana", 20));

        // The .add(-60) helps give the text a centered look to accommodate the different lengths of hello and world.
        hello.xProperty().bind(pane.widthProperty().divide(2).add(-60));
        hello.yProperty().bind(pane.heightProperty().divide(2));

        // Almost the exact same as hello except the actual text, color, and does not need to be moved right of center.
        Text world = new Text();
        world.setFill(Color.CORAL);
        world.setText("World!");
        world.setFont(Font.font("Verdana", 20));
        world.xProperty().bind(pane.widthProperty().divide(2));
        world.yProperty().bind(pane.heightProperty().divide(2));

        // Use the getChildren method to add the text to the pane and properly position it.
        pane.getChildren().add(hello);
        pane.getChildren().add(world);

        // Create a scene to place the pane on.
        Scene scene = new Scene(pane, 300, 300);

        primaryStage.setTitle("Hello World!");

        // Place the scene on the stage
        primaryStage.setScene(scene);

        // Make sure to show the scene.
        primaryStage.show();


    }
    // main function launches the stage-scene-pane-text window.
    public static void main(String[] args) {
        launch(args);
    }
}
