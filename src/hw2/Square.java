package hw2;

public class Square extends Shape{
    public Square(){
        side = 0;
    }
    public Square(double side){
        this.side = side;
    }

    // Return area of square, which is the side squared.
    public double getArea(){
        return side * side;
    }
}
