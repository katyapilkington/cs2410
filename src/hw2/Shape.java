package hw2;

/**
 * Shape is an abstract class as we need to know what sort of shape a given object is. Shape has four children: Squsre,
 * Cube, Triangle, and Pyramid.
 * @author Katya Pilkington
 */
abstract class Shape{
    // Each child of Shape has a double side value.
    double side;

    public double getSide() {
        return side;
    }

    public void setSide(double side) {
        this.side = side;
    }


    // Each child of Shape has an area method that returns a double area. For 3d shapes, area is the surface area.
    abstract double getArea();
}
