public class Person {
    private String name;
    private String address;
    private String phoneNumber;
    private String emailAddress;

    // Person class has no default(no arg) constructor as we don't want to be able to leave fields unfilled. All fields
    // are then used by child class objects.
    public Person(String name, String address, String phoneNumber, String emailAddress){
        this.name = name;
        this.address = address;
        this.phoneNumber = phoneNumber;
        this.emailAddress = emailAddress;
    }

    public String getName(){
        return name;
    }

    public String getAddress(){
        return address;
    }

    public String getPhoneNumber(){
        return phoneNumber;
    }

    public String getEmailAddress(){
        return emailAddress;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public void setEmailAddress(String emailAddress) {
        this.emailAddress = emailAddress;
    }

    // Overridden toString method returns the class (Person) and the name for the specific instantiated object.
    public String toString(){
        return "Person " + name;
    }
}
