public class Student extends Person {
    private String classStatus;

    // Student constructor includes fields from Person constructor as well as classStatus unique to this class.
    public Student(String classStatus, String name, String address, String phoneNumber, String emailAddress){
        super(name, address, phoneNumber, emailAddress);
        this.classStatus = classStatus;
    }

    public String getClassStatus(){
        return classStatus;
    }

    public void setClassStatus(String classStatus){
        this.classStatus = classStatus;
    }

    public String toString(){
        return "Student " + getName();
    }
}
