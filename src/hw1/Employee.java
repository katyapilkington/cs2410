public class Employee extends Person {
    private String office;
    private int salary;
    private MyDate dateHired;

    // Employee constructor has fields from Person constructor as well as office, salary, and year, month, day of year
    // hired (required for the MyDate constructor).
    public Employee(String office, int salary, int yearHired, int monthHired, int dayHired, String name, String address,
                    String phoneNumber, String emailAddress){
        super(name, address, phoneNumber, emailAddress);
        this.office = office;
        this.salary = salary;
        this.dateHired = new MyDate(yearHired, monthHired, dayHired);
    }

    public String getOffice(){
        return office;
    }

    public int getSalary(){
        return salary;
    }

    public String getDateHired(){
        return dateHired.toString();
    }

    public void setOffice(String office){
        this.office = office;
    }

    public void setSalary(int salary){
        this.salary = salary;
    }

    public void setDateHired(int year, int month, int day){
        dateHired.setYear(year);
        dateHired.setMonth(month);
        dateHired.setDay(day);
    }

    public String toString(){
        return "Employee " + getName();
    }
}
