public class Faculty extends Employee {
    private String officeHours;
    private String rank;

    // No default(no args) constructor as we need all information filled. Constructor includes fields needed for
    // Employee(and thus Person) constructor, as well as officeHours and rank which are unique to Faculty class.
    public Faculty(String officeHours, String rank, String office, int salary, int yearHired, int monthHired,
                   int dayHired, String name, String address, String phoneNumber, String emailAddress){
        super(office, salary, yearHired, monthHired, dayHired, name, address, phoneNumber, emailAddress);
        this.officeHours = officeHours;
        this.rank = rank;
    }

    public String getOfficeHours(){
        return officeHours;
    }

    public String getRank(){
        return rank;
    }

    public void setOfficeHours(String officeHours){
        this.officeHours = officeHours;
    }

    public void setRank(String rank){
        this.rank = rank;
    }

    public String toString(){
        return "Faculty " + getName();
    }
}
