/**
 * CS2410 Homework 1: 1/23/2019
 * @brief: Person class has children classes Student and Employee, with Staff and Faculty classes extending Employee.
 * In addition to setter and getter methods for data in each class, override the toString method in each class to
 * return the class and name of an object. Write a class MyDate with fields for year, month, and day to be used as the
 * type in the Employee dateHired field.
 * Write a TestPerson class to test the classes, including their constructors, a setter/getter pair, and the toString
 * methods.
 * @author katya pilkington
 */
public class TestPerson {
    public static void main(String[] args){
        // Initialize an instance of each of the classes using their constructors with made up data.
        // There are no default constructors due so that all information must be present.
        Person a = new Person("Alexander Worthwright", "1234 Somethington Street, Somewhere",
                "000-000-0000","alex.worthwrite@somemail.com");

        Student b = new Student("Freshman", "Lysander Williams",
                "4321 Bookington Lane, Somewhere", "000-111-1111",
                "lyles.the.ghost@somemail.com");

        Employee c = new Employee("LA101", 56000, 2050, 0, 29,
                "Maria Peterson", "5555 Somethington Street, Somewhere", "000-121-1212",
                "maria.peterson@somemail.com");

        Staff d = new Staff("Librarian", "LA202", 60000, 2047, 11, 10,
                "Mortimer Worthwright", "12344 Somethington Street, Somewhere", "000-101-1234"
                , "mortimer.worthwright@somemail.com");

        Faculty e = new Faculty("MWF 10:00am - 2:45pm", "Assistant Professor", "WCA302b",
                58000, 2060, 5, 3, "Lilian Ortega",
                "9785 N 4600 W, Sometown", "001-876-5679", "ortega.lilian@somemeail.com");

        MyDate f = new MyDate(2018, 3, 23);

        System.out.println("---Testing Classes---\n");
        System.out.println("---Expected Output---\nPerson Alexander Worthwright\nStudent Lysander Williams\n"
        + "Employee Maria Peterson\nStaff Mortimer Worthwright\nFaculty Lilian Ortega\n3-23-2018\n");

        // Test each class's toString method to compare with expected output. Each class (except MyDate) has an
        // overridden toString method that returns the object's class and name.
        System.out.println("---Actual Output---");
        System.out.println(a.toString() + "\n" + b.toString() + "\n" + c.toString() + "\n" + d.toString() + "\n" +
                e.toString() + "\n" + f.toString() + "\n");

        // Test a setter and getter method set for a data parameter for each class and use print statements to show that
        // the set method worked as expected.
        System.out.println("---Testing Setters and Getters---\n");
        System.out.println("Class Person\nphone number before change: " + a.getPhoneNumber());
        a.setPhoneNumber("000-123-4567");
        System.out.println("phone number after change: " + a.getPhoneNumber() + "\n");

        System.out.println("---Student Class---\nclass status before change: " + b.getClassStatus());
        b.setClassStatus("Junior");
        System.out.println("class status after change: " + b.getClassStatus() + "\n");

        System.out.println("---Employee Class---\nstatus before change: " + c.getOffice());
        c.setOffice("FAV201");
        System.out.println("status after change: " + c.getOffice() + "\n");

        System.out.println("---Staff Class---\nstatus before change: " + d.getTitle());
        d.setTitle("Metadata Supervisor");
        System.out.println("status after change: " + d.getTitle() + "\n");

        System.out.println("---Faculty Class---\noffice hours before change: " + e.getOfficeHours());
        e.setOfficeHours("MWF 1:30-3:45pm");
        System.out.println("office hours after change: " + e.getOfficeHours() + "\n");

        System.out.println("---MyDate Class---\nyear before change: " + f.getYear());
        f.setYear(2019);
        System.out.println("year after change: " + f.getYear());
        }
}
