public class Staff extends Employee {
    private String title;

    // Constructor takes takes in information for Employee(and Person by extension) constructors as well as title, which
    // is unique to Staff class.
    public Staff(String title, String office, int salary, int yearHired, int monthHired, int dayHired, String name,
                 String address, String phoneNumber, String emailAddress){
        super(office, salary, yearHired, monthHired, dayHired, name, address, phoneNumber, emailAddress);
        this.title = title;
    }

    public String getTitle(){
        return title;
    }

    public void setTitle(String title){
        this.title = title;
    }

    public String toString(){
        return "Staff " + getName();
    }
}
