package hw4;

import javafx.animation.KeyFrame;
import javafx.animation.Timeline;
import javafx.application.Application;
import javafx.beans.property.IntegerProperty;
import javafx.geometry.Insets;
import javafx.scene.Scene;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.StackPane;
import javafx.scene.paint.Color;
import javafx.scene.shape.Circle;
import javafx.scene.shape.Rectangle;
import javafx.stage.Stage;
import javafx.util.Duration;

import javax.sound.midi.Soundbank;

/**
 * CS2410 Homework 4: Game Connect Four Programming Exercise 14.5
 * Allows two players to play connect four on a GUI. To place a disk, hover over the chosen column
 * above the board (white space at top of window) and click on the circle there. The program flashes
 * the four winning cells. Reports no winners if all cells are occupied with no winners.
 * @author Katya Pilkington
 */
public class ConnectFour extends Application {
    private int[][] gameArray;
    private GridPane gameBoard;
    private Circle[][] circleBoard;
    private int playerTurn = 1; //keep track of whose turn it is.
    private Color player1Color = Color.CORAL;   // easily change player colors
    private Color player2Color = Color.CORNFLOWERBLUE; // easily change player colors

    // Default constructor initializes all arrays and boards.
    public ConnectFour(){
        initializeGameArray();
        initializeGameBoard();
    }

    // Initialize a 7x6 grid pane with white circles to form the GUI's board. Bind each circle's fill
    // with the fill of another circle that is in an array similar to the integer array of gameArray, which
    // will allow us to change the colors on the board when moves are made.
    private void initializeGameBoard(){
        circleBoard = new Circle[6][7];
        gameBoard = new GridPane();
        gameBoard.setHgap(5);
        gameBoard.setVgap(5);
        gameBoard.setPadding(new Insets(5));
        for(int i = 0; i < 6; i++){
            for(int j = 0; j < 7; j++){
                Circle gameCircle = new Circle(25);
                Circle storedCircle = new Circle(1, Color.WHITE);
                gameCircle.fillProperty().bind(storedCircle.fillProperty());
                gameBoard.add(gameCircle, j, i);
                circleBoard[i][j] = storedCircle;
            }
        }
    }

    // Form an array of integers to check for winning moves. Initialized so all entries are 0, but
    // will change to 1 or 2 depending on which player adds a token in that spot.
    private void initializeGameArray(){
        gameArray = new int[6][7];
        for(int i = 0; i < 6; i++){
            for(int j = 0; j < 7; j++){
                gameArray[i][j] = 0;
            }
        }
    }

    // Forms the GUI built out of a border pane. Calls logic to form the game board and grid pane
    // where moves are made.
    public void start(Stage primaryStage){
        BorderPane borderPane = new BorderPane();

        StackPane stackPane = new StackPane();
        Rectangle background = new Rectangle(390, 335, Color.SLATEGRAY);
        stackPane.getChildren().add(background);
        stackPane.getChildren().add(gameBoard);

        borderPane.setCenter(stackPane);

        StackPane stack2 = new StackPane();
        Rectangle rectangle1 = new Rectangle(390, 60, Color.WHITE);
        GridPane grid2 = makePlayingBoard();

        stack2.getChildren().add(rectangle1);
        stack2.getChildren().add(grid2);

        borderPane.setTop(stack2);


        Scene scene = new Scene(borderPane);
        primaryStage.setScene(scene);
        primaryStage.setTitle("Connect Four");
        primaryStage.show();
    }


    // Initialize the board of 7 circles above the game board that allows players to hover over columns
    // as the method of making moves. Circles change color to whose player's turn it is when entered,
    // go back to white when exited, and when clicked makes a move on that column.
    private GridPane makePlayingBoard(){
        GridPane playingBoard = new GridPane();
        playingBoard.setVgap(5);
        playingBoard.setHgap(5);
        playingBoard.setPadding(new Insets(5));

        for(int i = 0; i < 7; i++){
            Circle circle = new Circle(25, Color.WHITE);
            Integer column = i;
            circle.setOnMouseEntered(e-> changeCircleColor(circle));
            circle.setOnMouseExited(e -> circle.setFill(Color.WHITE));
            circle.setOnMouseClicked(e -> makeAMove(column, circle));
            playingBoard.add(circle, i,0);
        }
        return playingBoard;
    }

    // Changes a circle (from circleBoard) to the color given by which player's move it is.
    private void changeCircleColor(Circle circle){
        if(playerTurn ==1) {
            circle.setFill(player1Color);
        }
        else{
            circle.setFill(player2Color);
        }
    }

    // Logic run after a player clicks on a circle above a column. Checks for invalid move, then
    // adds the token, changes gameArray to match. Check for winning move each time.
    private void makeAMove(int column, Circle circle){
        int row = nextAvaliableRow(column);
        if(row == -1){
            System.out.println("Token cannot be placed here. Try a different column.");
            return;
        }
        addToken(row, column);
        if(checkWin()){

            System.out.println("Player" + playerTurn + " wins!");
            return;
        }

        if(playerTurn == 1){
            playerTurn = 2;
            circle.setFill(player2Color);
        }
        else{
            playerTurn = 1;
            circle.setFill(player1Color);
        }
    }


    // Finds the next spot for a token based on a column chosen.
    public int nextAvaliableRow(int column){
        if (gameArray[0][column] != 0){
            return -1;      // entire column filled; not a valid move
        }
        int row = 0;
        boolean filled = false;
        while(!filled){
            if(gameArray[row + 1][column] == 0){
                row += 1;
                if(row >= 5){       // Make sure not to go out of bounds.
                    filled = true;
                }
            }
            else{filled = true;}
        }
        return row;
    }

    // Changes the color of a circle on the board to match move.
    private void addToken(int row, int column){
        gameArray[row][column] = playerTurn;
        if(playerTurn == 1){
            circleBoard[row][column].setFill(player1Color);
        }
        else{
            circleBoard[row][column].setFill(player2Color);
        }
    }

    // Logic for checking if 4 of the same color tokens are in a row.
    public boolean checkWin(){
        // Check for vertical win
        for(int i = 0; i < 3; i++){
            for(int j = 0; j < 7; j++){
                if((gameArray[i][j] != 0)  &&
                        (gameArray[i][j] == gameArray[i+1][j]) &&
                        (gameArray[i+1][j] == gameArray[i+2][j]) &&
                        (gameArray[i+2][j] == gameArray[i+3][j])){
                    flashWinningTokens(i, j, i+1, j, i+2, j, i+3, j);
                    return true;
                }
            }
        }

        // Check for horizontal win.
        for(int i = 0; i < 6; i++) {
            for (int j = 0; j < 4; j++) {
                if ((gameArray[i][j] != 0) &&
                        (gameArray[i][j] == gameArray[i][j + 1]) &&
                        (gameArray[i][j + 1] == gameArray[i][j + 2]) &&
                        (gameArray[i][j + 2] == gameArray[i][j + 3])) {
                    flashWinningTokens(i, j, i, j+1, i, j+2, i, j+3);
                    return true;
                }
            }
        }

        // Check for downwards diagonal win.
        for(int i = 0; i < 3; i++) {
            for (int j = 0; j < 4; j++) {
                if ((gameArray[i][j] != 0) &&
                        (gameArray[i][j] == gameArray[i + 1][j + 1]) &&
                        (gameArray[i][j] == gameArray[i + 2][j + 2]) &&
                        (gameArray[i][j] == gameArray[i + 3][j + 3])) {
                    flashWinningTokens(i, j, i+1, j+1, i+2, j+2, i+3, j+3);
                    return true;
                }
            }
        }

        // Check for upwards diagonal win.
        for(int i = 3; i < 6; i++) {
            for (int j = 0; j < 4; j++) {
                if ((gameArray[i][j] != 0) &&
                        (gameArray[i][j] == gameArray[i - 1][j + 1]) &&
                        (gameArray[i][j] == gameArray[i - 2][j + 2]) &&
                        (gameArray[i][j] == gameArray[i - 3][j + 3])) {
                    flashWinningTokens(i, j, i-1, j+1, i-2, j+2, i-3, j+3);
                    return true;
                }
            }
        }

        // check to make sure the board is not full with no winners
        for(int i = 0; i < 7; i++){
            for(int j = 0; j < 6; j++){
                if(gameArray[i][j] == 0)
                    return false;
            }
        }
        // No wins detected.
        System.out.println("It' a tie. No winners.");

        return false;
    }

    // Changes the color on the 4 winning tokens every 100 milliseconds to imitate "flashing"
    private void flashWinningTokens(
            int row1, int column1, int row2, int column2, int row3, int column3, int row4, int column4){
        Timeline animation = new Timeline(new KeyFrame(Duration.millis(100), e -> {
            Color changeColor = Color.rgb((int)(Math.random() * 255), (int)(Math.random() * 255), (int)(Math.random() * 255));
            circleBoard[row1][column1].setFill(changeColor);
            circleBoard[row2][column2].setFill(changeColor);
            circleBoard[row3][column3].setFill(changeColor);
            circleBoard[row4][column4].setFill(changeColor);
        }));
        animation.setCycleCount(Timeline.INDEFINITE);
        animation.play();
    }


    // Creates an instance of ConnectFour game to run.
    public static void main(String[] args) {
        ConnectFour game = new ConnectFour();
        launch(args);
    }
}
